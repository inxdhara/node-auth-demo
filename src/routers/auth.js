const express = require('express');
const router = new express.Router();
const authController = require('./../controllers/AuthController');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

router.post('/register', jsonParser, authController.userRegister);
router.post('/login', jsonParser, authController.userLogin);

module.exports = router;