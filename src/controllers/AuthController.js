const jwt = require('jsonwebtoken');
var crypto = require('crypto');

const User = require("./../models/users");
const config = require( "./../config" );

const logger = require( "./../utilities/logger" );

/**
 * Used for do register for user
 * @param {*} req 
 * @param {*} res 
 */
const userRegister = (req, res) => {
    var cipher = crypto.createCipher(config.cryptoAlgo, config.cryptoKey);
    var encrypted = cipher.update(req.body.password, 'utf8', 'hex')+cipher.final('hex');

    const user = new User({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        email: req.body.email,
        address: req.body.address,
        password: encrypted,
    });

    user.save()
    .then(result => {
        jwt.sign({result}, config.jwtSecret, {expiresIn: '300s'}, (err, token) => {
            res.success(token);
        });
    })
    .catch(err => {
        console.log(err);
        res.send( err );
    });
  }

/**
 * Used for do login for user
 * @param {*} req 
 * @param {*} res 
 */
const userLogin = (req, res) => {
    User.findOne({email: req.body.email})
    .then((result) => {
        if (result) {
            var decipher = crypto.createDecipher(config.cryptoAlgo, config.cryptoKey);
            var decrypted = decipher.update(result.password, 'hex', 'utf8') + decipher.final('utf8');
            console.log(decrypted)
            if (decrypted ===  req.body.password) {
                jwt.sign({result}, config.jwtSecret, {expiresIn: '300s'}, (err, token) => {
                    res.success(token);
                });
                logger.info('Login successfully');
            } else {
                res.unauthorized();
            }
        } else {
            res.status(500).json({
                status: false,
                message: 'Invalid user or user not exist'
            })
        }
    })
    .catch(err => {
        res.send( err );
    });
}

  module.exports = {
    userRegister,
    userLogin 
  }
