const express = require('express');
const app = express();
var bodyParser = require('body-parser');
const config = require( "./config" );
const customResponses = require( "./middlewares/customResponses" );
const logger = require( "./utilities/logger" );

const authRouter = require('./routers/auth');
const userRouter = require('./routers/users');

const port = process.env.PORT || config.port;
const ENV = process.env.NODE_ENV || config.env;

app.set( "env", ENV );

app.use( bodyParser.json( ) );
app.use( customResponses );

require( "./config/mongoose" )( app );

app.use( "/", authRouter );
app.use( "/user", userRouter ); 

app.use( ( req, res ) => {
    res.notFound( );
} );

app.use( ( err, req, res, next ) => {
    logger.error( err.stack );
    next( err );
} );

// Don't remove next !!!!
app.use( ( err, req, res, next ) => {
    res.status( 503 ).json( {
        success: false,
        error: "server_error",
    } );
} );

app.listen( port, ( ) => {
    console.log( `Listening on port ${ port }` );
} );