const express = require('express');
const router = new express.Router();
const UserController = require('./../controllers/UserController');
const verifyToken = require( "./../middlewares/verifyToken" );

router.get('/getAll', verifyToken,  UserController.getAllUsersList);

module.exports = router;