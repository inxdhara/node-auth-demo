const User = require("./../models/users");

const getAllUsersList = (req, res) => {
    User.find().then((data) => {
        res.success(data);
    })
}

module.exports = {
    getAllUsersList,
  }